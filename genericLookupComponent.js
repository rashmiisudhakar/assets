import lookUp from '@salesforce/apex/GenericUtils.search';
import { api, LightningElement, track, wire } from 'lwc';
 
 
export default class GenericLookupComponent extends LightningElement {
 
   @api objName = '';
   @api iconName;
   @api filter = '';
   @api searchPlaceholder='Search';
   @api searchKey = 'Name';
   @api filterfields = '';
   @api isrequired = false;
   @api fieldlabel ;
   @api selectedRecordName = '';
   @track selectedName = '';
   @track records;
   @track isValueSelected;
   @track blurTimeout;
   @track searchTerm;
   @track selectedRecord;
   @track showError = false;
   //css
   @track boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
   @track inputClass = '';
 
 
   connectedCallback(){
     if(this.selectedRecordName && this.selectedRecordName.length > 1){
         this.selectedName = this.selectedRecordName ;
         this.isValueSelected = true;
         this.showError = false;
         this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
     }
   }
   @api
   validateInput() {
       if (this.isrequired) {
           if (this.isValueSelected) {
               this.showError = false;
               return true;
           } else {
               this.showError = true;
               this.inputClass = 'slds-has-error error';
               return false;
           }
       } else {
           return true;
       }
   }
   handleClick() {
       this.searchTerm = '';
       this.inputClass = 'slds-has-focus';
       this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus slds-is-open';
   }
   onBlur() {
       this.blurTimeout = setTimeout(() =>  {this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus'}, 300);
   }
   onSelect(event) {
       let selectedId = event.currentTarget.dataset.id;
       let selectedName = event.currentTarget.dataset.name;
       this.records.some(selectedRecordData => {
           console.log(JSON.stringify(selectedRecordData));
           if (selectedRecordData.Id === selectedId) {
               this.selectedRecord = selectedRecordData;
               const valueSelectedEvent = new CustomEvent('lookupselected', {
                   detail: {
                       selectedRecord: this.selectedRecord,
                   }
               });
               this.dispatchEvent(valueSelectedEvent);
           }
       });
       this.isValueSelected = true;
       this.showError = false;
       this.selectedName = selectedName;
       if(this.blurTimeout) {
           clearTimeout(this.blurTimeout);
       }
       this.boxClass = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-has-focus';
   }
 
   handleRemovePill() {
       this.isValueSelected = false;
       this.selectedRecord = null;
       const valueSelectedEvent = new CustomEvent('lookupselected', {
           detail: {
               selectedRecord: this.selectedRecord,
           }
       });
       this.dispatchEvent(valueSelectedEvent);
   }
 
   onChange(event) {
       this.searchTerm = event.target.value;
       this.fetchRecords();
   }
  
   fetchRecords(){
       lookUp({
           searchTerm : this.searchTerm,
           myObject : this.objName,
           filter : this.filter,
           filterFields : this.filterfields,
           searchKey :  this.searchKey
       }).then(response => {
           this.error = undefined;
           this.records = response;
       }).catch(error => {
           console.log('error>>>' + JSON.stringify(error));
           // console.log("Error: " + (error.message || error.body.message));
       });
   }
  
 
}
