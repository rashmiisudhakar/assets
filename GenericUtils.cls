public class GenericUtils {
@AuraEnabled(cacheable=true)
   public static List<sObject> search(String searchTerm, string myObject, String filter,String filterFields,String searchKey) {
       if(searchTerm == null || searchTerm.length() <= 2){
           return null;
       }
       String myQuery = null;
       if(String.isNotBlank(filterFields)){
           myQuery = 'Select '+filterFields+ ' from '+ myObject;
       }
       else{
           myQuery = 'Select Id, Name from ' + myObject;
       }
       if(String.isNotBlank(searchKey)){
            myQuery =  myQuery + ' Where '+ searchKey +' Like  \'%' + searchTerm + '%\'';
       }
       else{
           myQuery =  myQuery + ' Where Name Like  \'%' + searchTerm + '%\'';
       }
       if(filter != null && filter != ''){
           myQuery = myQuery + ' AND '+filter+' LIMIT  100';
       }
       else {
            myQuery =  myQuery +' LIMIT  100';
       }
       List<sObject> lookUpList = database.query(myQuery);
       return lookUpList;
   }
}
